const importAll = function(r) {
  r.keys().forEach(r)
}

// importAll(require.context("./js", true, /\.js$/))
// importAll(require.context("./css", true, /\.css$/))

// import "./css/klient/style.css"
import "./css/jquery.prettyPhoto.css"
// import "./css/w4_blue2.css"

// window.$ = window.jQuery = jQuery

import "./js/jquery.prettyPhoto"
import "./js/dev/classes"
// import "./js/klient/main"
