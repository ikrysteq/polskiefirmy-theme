/**********************
 *   DEVELOPMENT ONLY
 *********************/

document.addEventListener("DOMContentLoaded", function() {
  // ukrycie dolnego kontaktu (widoczny tylko u gory)
  let kontaktDolElement = document.getElementById("kontaktDol")
  kontaktDolElement ? kontaktDolElement.classList.add("hidden") : {}

  var buttonStyle = {
    width: "auto",
    height: "65px",
    margin: "10px 20px",
    padding: "5px",
    fontSize: "16px",
    borderRadius: "10px"
  }

  var controlsDiv = document.createElement("div")
  controlsDiv.style.width = "150px"
  controlsDiv.style.height = "500px"
  controlsDiv.style.background = "grey"
  controlsDiv.style.color = "white"
  controlsDiv.style.position = "fixed"
  controlsDiv.style.bottom = "0"
  controlsDiv.style.left = "auto"
  controlsDiv.style.right = "0"
  controlsDiv.classList.add("control-theme")

  // DEFINED FONTS
  var fontIndex = 0
  var fontsDefs = [
    { name: "Helvetica", class: "font-helvetica" },
    { name: "Open Sans", class: "font-opensans" },
    { name: "Oswald", class: "font-oswald" },
    { name: "Releway", class: "font-releway" }
  ]

  // DEFINED TEXT COLORS
  var colorIndex = 0
  var textColors = [
    "text-yellow",
    "text-blue",
    "text-darkblue",
    "text-marine",
    "text-green",
    "text-lightgreen",
    "text-purple",
    "text-orange",
    "text-red"
  ]

  var pullPictureTrigger = false
  var buttons = []
  var buttonsDefs = [
    {
      name: "1. styl jasny/ciemny (LOGO)",
      event: function() {
        // dodac .with-logo na #calosc
        let caloscElement = document.getElementById("calosc")
        caloscElement.classList.contains("with-logo")
          ? caloscElement.classList.remove("with-logo")
          : caloscElement.classList.add("with-logo")
      }
    },
    {
      name: "3. sekcja kontakt góra/dół",
      event: function() {
        // kontakt w #kontaktGora lub kontaktDol
        let kontaktGoraElement = document.getElementById("kontaktGora")
        let kontaktDolElement = document.getElementById("kontaktDol")
        kontaktGoraElement.classList.contains("hidden")
          ? kontaktGoraElement.classList.remove("hidden")
          : kontaktGoraElement.classList.add("hidden")
        kontaktDolElement.classList.contains("hidden")
          ? kontaktDolElement.classList.remove("hidden")
          : kontaktDolElement.classList.add("hidden")
      }
    },
    {
      name: "4. zdjecia lewo/prawo",
      event: function() {
        // sekcja z div.container o klasie .with-picture może przyjąacklasy .pull-left / .pull-right
        let sectionArr = document.querySelectorAll(".container.with-picture")

        sectionArr.forEach(element => {
          pullPictureTrigger && element.classList.contains("pull-right")
            ? element.classList.remove("pull-right")
            : element.classList.add("pull-right")
          !pullPictureTrigger && element.classList.contains("pull-left")
            ? element.classList.remove("pull-left")
            : element.classList.add("pull-left")
        })

        pullPictureTrigger = !pullPictureTrigger
      }
    },
    {
      name: "5. zmiana koloru (H1, H3)",
      event: function() {
        colorIndex >= textColors.length ? (colorIndex = 0) : {}

        // wybrac selektor dla stylowania tekstu w #main
        let textArr = document.querySelectorAll(
          "#main h1, #main h3, .nazwa-zakladki"
        )

        textArr.forEach(element => {
          for (let index = 0; index < textColors.length; index++) {
            element.classList.contains(textColors[index])
              ? element.classList.remove(textColors[index])
              : {}
          }
        })

        textArr.forEach(element => {
          element.classList.add(textColors[colorIndex])
        })

        colorIndex < textColors.length ? colorIndex++ : {}
      }
    },
    {
      name: "6*. zmiana czcionki (H1, H3)",
      event: function() {
        fontIndex >= fontsDefs.length ? (fontIndex = 0) : {}

        // wybrac selektor dla stylowania tekstu w #main
        let textArr = document.querySelectorAll(
          "#main h1, #main h3, .nazwa-zakladki"
        )
        textArr.forEach(element => {
          for (let index = 0; index < fontsDefs.length; index++) {
            element.classList.contains(fontsDefs[index].class)
              ? element.classList.remove(fontsDefs[index].class)
              : {}
          }
        })

        textArr.forEach(element => {
          element.classList.add(fontsDefs[fontIndex].class)
        })

        fontIndex < fontsDefs.length ? fontIndex++ : {}
      }
    }
  ]

  // show buttons in control panel
  for (let index = 0; index < buttonsDefs.length; index++) {
    buttons[index] = document.createElement("button")
    buttons[index].innerHTML = buttonsDefs[index].name
    buttons[index].style.width = buttonStyle.width
    buttons[index].style.height = buttonStyle.height
    buttons[index].style.margin = buttonStyle.margin
    buttons[index].style.padding = buttonStyle.padding
    buttons[index].style.fontSize = buttonStyle.fontSize
    buttons[index].style.borderRadius = buttonStyle.borderRadius
    buttons[index].addEventListener("click", buttonsDefs[index].event)
    controlsDiv.appendChild(buttons[index])
  }

  document.querySelector("body").appendChild(controlsDiv)
})
