var nameValidated = false,
  companyNameValidated = false,
  phoneValidated = false,
  emailValidated = false,
  termsAccepted = false

$(function() {
  $("nav ul li:last-child").css("box-shadow", "0 0 0")
  $("a[rel^='prettyPhoto']").prettyPhoto()

  var left = $(document).outerWidth() - $(window).width()
  $("body, html").scrollLeft(left)

  // Nav Scroll
  $(window).on("scroll", function() {
    var scroll = $(window).scrollTop()

    if ($(".with-logo").length) {
      if (scroll >= 168) {
        $("#nav").addClass("fixed")
        $("#baner").addClass("fixed")
      } else {
        $("#nav").removeClass("fixed")
        $("#baner").removeClass("fixed")
      }
    } else {
      if (scroll >= 50) {
        $("#nav").addClass("fixed")
        $("#baner").addClass("fixed")
      } else {
        $("#nav").removeClass("fixed")
        $("#baner").removeClass("fixed")
      }
    }
  })

  $(".showValueAnalytics").each(function() {
    var eventLabel = $(this).data("name"),
      fieldValue = $(this).data("value"),
      value = []

    value.push(fieldValue.slice(0, 2))
    value.push(
      fieldValue
        .replace(new RegExp("[0-9]", "g"), "X")
        .substring(2, fieldValue.length)
    )
    fieldValue = value[0].concat(value[1])

    $(this).append("<span>" + fieldValue + "</span>")
    $(this).append('<span class="spoiler">Pokaż</span>')
  })

  $(".showValueAnalytics").click(function() {
    var eventLabel = $(this).data("name"),
      fieldValue = $(this).data("value")

    ga("send", "event", "Conversion", "Click", eventLabel, 10)

    $(this)
      .children("span.spoiler")
      .remove()
    $(this)
      .children("span")
      .text(fieldValue)
    $(this).removeClass("showValueAnalytics")
  })

  $(".sendEventAnalytics").click(function() {
    var $this = $(this),
      href = $this.data("href"),
      eventName = $this.data("name")

    ga("send", "event", "Conversion", "Click", eventName, 10)

    setTimeout(function() {
      window.location = href
    }, 300)

    return false
  })

  $("#formName").on("input", function() {
    var regexp = /^.{3,}$/
    nameValidated = regexp.test($(this).val())
    validateForm()
  })

  $("#formCompanyName").on("input", function() {
    var regexp = /^.{3,}$/
    companyNameValidated = regexp.test($(this).val())
    validateForm()
  })

  $("#formPhone").on("input", function() {
    var regexpDigit = /^[0-9+()\\/ -]+$/gi
    phoneValidated = regexpDigit.test($(this).val())
    validateForm()
  })

  $("#formEmail").on("input", function() {
    var regexpEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    emailValidated = regexpEmail.test($(this).val())
    validateForm()
  })

  $("#contactForm").on("change", ":checkbox", function() {
    termsAccepted = $(this).is(":checked")
    validateForm()
  })

  $("#submitButton").click(function() {
    if (!validateForm()) {
      return
    }

    $(this).attr("disabled", "")
    $(this).text("WYSYŁANIE...")

    // Send the data using post with element ids
    var posting = $.post("send_contact_form.php", {
      name: $("#formName").val(),
      company: $("#formCompanyName").val(),
      email: $("#formEmail").val(),
      content: $("#formContent").val()
    })

    // Alerts the results
    posting.done(function(data) {
      $(this).removeAttr("disabled")
      $(this).text("Zamawiam kontakt")
      alert(data)
    })
  })
})

function hide() {
  element = document.getElementById("cookies-info")
  element.style.display = "none"
  var client = new XMLHttpRequest()
  client.open("POST", "ajax.php", false)
  client.send(null)
}

function validateForm() {
  if (
    nameValidated &&
    companyNameValidated &&
    phoneValidated &&
    termsAccepted
  ) {
    $("#submitButton").removeAttr("disabled")
    return true
  } else {
    $("#submitButton").attr("disabled", "")
  }
  return false
}
