const path = require("path")
const webpack = require("webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const HtmlWebpackHarddiskPlugin = require("html-webpack-harddisk-plugin")
const CopyWebpackPlugin = require("copy-webpack-plugin")
const HtmlBeautifyPlugin = require("html-beautify-webpack-plugin")

// const MiniCssExtractPlugin = require("mini-css-extract-plugin")

const devMode = process.env.NODE_ENV !== "production"
// Constant with our paths
const paths = {
  // BUILD: path.resolve(__dirname, "build"),
  SRC: path.resolve(__dirname, "src")
  // JS: path.resolve(__dirname, "src/js")
}

// Webpack configuration
module.exports = {
  entry: path.join(paths.SRC, "index.js"),
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "app.js"
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: "ofirmie.html",
      template: path.join(paths.SRC, "ofirmie.html"),
      alwaysWriteToDisk: true
    }),
    new HtmlWebpackPlugin({
      filename: "oferta.html",
      template: path.join(paths.SRC, "oferta.html"),
      alwaysWriteToDisk: true
    }),
    new HtmlWebpackPlugin({
      filename: "produkty.html",
      template: path.join(paths.SRC, "produkty.html"),
      alwaysWriteToDisk: true
    }),
    new HtmlWebpackPlugin({
      filename: "marki.html",
      template: path.join(paths.SRC, "marki.html"),
      alwaysWriteToDisk: true
    }),
    new HtmlWebpackPlugin({
      filename: "kontakt.html",
      template: path.join(paths.SRC, "kontakt.html"),
      alwaysWriteToDisk: true
    }),
    new HtmlBeautifyPlugin({
      config: {
        html: {
          end_with_newline: true,
          indent_size: 1,
          indent_with_tabs: true,
          indent_inner_html: true,
          preserve_newlines: true
        }
      }
    }),
    new HtmlWebpackHarddiskPlugin({
      outputPath: path.resolve(__dirname, "dist")
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
    new CopyWebpackPlugin([
      {
        from: "./src/files",
        to: "./files"
      },
      {
        from: "./src/css",
        to: "./css"
      },
      {
        from: "./src/js",
        to: "./js",
        ignore: devMode ? "" : "classes.js"
      }
    ])
  ],
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.(html)$/,
        use: [
          {
            loader: "html-loader",
            options: {
              minimize: false,
              interpolate: "require",
              attrs: false
            }
          },
          {
            loader: "html-component-loader",
            options: {
              src: __dirname
            }
          }
        ]
      },
      {
        test: /\.(png|jp(e*)g|svg|gif)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path][name].[ext]"
            }
          }
        ]
      }
    ]
  },
  externals: {
    jquery: "jQuery"
  },
  devServer: {
    port: 8001,
    contentBase: path.join(__dirname, "dist")
  }
}
